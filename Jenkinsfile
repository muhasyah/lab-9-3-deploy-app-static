pipeline {
    agent none
    environment {
        DOCKER_IMAGE = 'lab9-3-nginx:latest'
        CONTAINER_NAME = 'lab-93'
        REPO_URL = 'https://gitlab.com/muhasyah/lab-9-3-deploy-app-static.git' // Gantilah dengan URL repo Anda
        REPO_BRANCH = 'main' // Gantilah dengan branch yang sesuai
    }
    stages {
        stage('Clone Repository') {
            agent {
                label 'development'
            }
            steps {
                git branch: "${env.REPO_BRANCH}", url: "${env.REPO_URL}"
            }
        }
        stage('Check and Remove Existing Image') {
            agent {
                label 'development'
            }
            steps {
                script {
                    def imageExists = sh(script: "docker images -q ${DOCKER_IMAGE}", returnStdout: true).trim()
                    if (imageExists) {
                        sh "docker rmi -f ${DOCKER_IMAGE}"
                    }
                }
            }
        }
        stage('Build Docker Image') {
            agent {
                label 'development'
            }
            steps {
                script {
                    sh 'docker build -t ${DOCKER_IMAGE} .'
                }
            }
        }
        stage('Check and Remove Existing Container') {
            agent {
                label 'development'
            }
            steps {
                script {
                    def containerExists = sh(script: "docker ps -a --filter name=${env.CONTAINER_NAME} --format '{{.Names}}'", returnStdout: true).trim()
                    if (containerExists == "${env.CONTAINER_NAME}") {
                        sh "docker stop ${env.CONTAINER_NAME}"
                        sh "docker rm ${env.CONTAINER_NAME}"
                    }
                }
            }
        }
        stage('Run New Container') {
            agent {
                label 'development'
            }
            steps {
                script {
                    sh "docker run -d --name ${env.CONTAINER_NAME} -p 8070:80 ${env.DOCKER_IMAGE}"
                    sh "curl -i localhost:8070"
                }
            }
        }
        stage('Check Image for Production') {
            when {
                expression {
                    currentBuild.result == null || currentBuild.result == 'SUCCESS'
                }
            }
            agent {
                label 'node-production'
            }
            steps {
                script {
                    def imageExists = sh(script: "docker images -q ${DOCKER_IMAGE}", returnStdout: true).trim()
                    if (imageExists) {
                        sh "docker rmi -f ${DOCKER_IMAGE}"
                    }
                }
            }
        }
        stage('Build Docker Image for Production') {
            when {
                expression {
                    currentBuild.result == null || currentBuild.result == 'SUCCESS'
                }
            }
            agent {
                label 'node-production'
            }
            steps {
                script {
                    git branch: "${env.REPO_BRANCH}", url: "${env.REPO_URL}" // Clone repository again for production build
                    sh 'docker build -t ${DOCKER_IMAGE} .'
                }
            }
        }
        stage('Deploy to Production') {
            when {
                expression {
                    currentBuild.result == null || currentBuild.result == 'SUCCESS'
                }
            }
            agent {
                label 'node-production'
            }
            steps {
                script {
                    def prodContainerExists = sh(script: "docker ps -a --filter name=${CONTAINER_NAME} --format '{{.Names}}'", returnStdout: true).trim()
                    if (prodContainerExists == "${CONTAINER_NAME}") {
                        sh "docker stop ${CONTAINER_NAME}"
                        sh "docker rm ${CONTAINER_NAME}"
                    }
                    sh "docker run -d --name ${CONTAINER_NAME} -p 8080:80 ${DOCKER_IMAGE}"
                }
            }
        }
    }
}
